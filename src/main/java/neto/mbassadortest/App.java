package neto.mbassadortest;

import net.engio.mbassy.bus.MBassador;
import net.engio.mbassy.bus.config.BusConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        log.info("Início!");

        // create as many instances as necessary
        // bind it to any upper bound
        MBassador<TestMessage> bus = new MBassador<>(BusConfiguration.Default());
        ListeningBean listener = new ListeningBean();
        // the listener will be registered using a weak-reference if not configured otherwise with @Listener
        bus.subscribe(listener);

        bus.publish(new TestMessage("Neto 1"));
        bus.publish(new TestMessage("Neto 2"));
        bus.publish(new TestMessage("Neto 3"));
        bus.publish(new TestMessage("Neto 4"));
    }
}
