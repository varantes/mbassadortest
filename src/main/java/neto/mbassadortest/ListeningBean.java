/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.mbassadortest;

import net.engio.mbassy.listener.Handler;
import net.engio.mbassy.listener.Invoke;
import net.engio.mbassy.listener.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
@Listener
public class ListeningBean {

    private static final Logger log = LoggerFactory.getLogger(ListeningBean.class);

    // every message of type TestMessage or any subtype will be delivered
    // to this handler
    @Handler(delivery = Invoke.Asynchronously)
    public void handleTestMessage(TestMessage message) {
        log.info("message={}", message);
    }
}
