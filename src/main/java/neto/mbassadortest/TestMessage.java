/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neto.mbassadortest;

/**
 *
 * @author valdemar.arantes
 */
public class TestMessage {

    private String name;

    public TestMessage() {
        name = "Indefinido";
    }

    ;

    public TestMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "TestMessage{" + "name=" + name + '}';
    }

}
